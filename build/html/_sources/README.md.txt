# Pods creation

This is a fast explanation for create your own pod library from scratch, push it to a private repo and use it.

## Install CocoaPods

As you might think, CocoaPods is a Ruby gem which means it can be installed using the following command:

```ruby
$ sudo gem install cocoapods
```

## Create Pod Library

Now, you can execute next line to create your pod library.

```ruby
$ pod lib create YOUR_LIBRARY_NAME
```

Once this command is entered, you will get asked a series of questions.

* First, we have to choose wich platform we want to use, answer `iOS`
* When you’re asked what language you want to use, answer `Swift`.
* When asked if you’d like to include a demo application, answer`Yes`.
* When asked which testing framework to use, answer `None`.
* Answer `No` to the prompt regarding view based testing.

When the creation process had finished, the pod project will be opened in XCode if everything goes ok. If your pod project
could not opened in XCode, you can open it manually looking into your project the `Example` folder, then inside you must find a `.workspace` file, open the project from it.

You can view the whole project structure, open the `.podspec` file located into the `Podspec Metadata` group folder, for start the pod setup.

![Podspec location](https://s3.amazonaws.com/shareproximateapps/doc/ios/1.png)

## Setting up the podspec info

At this point, our pod library has been created, but we have to completing some info before start coding.
a `.podspec` file are located on the root pod folder, we need to add some info to this file.

```ruby
#
# Be sure to run `pod lib lint library.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'library'
  s.version          = '0.1.0'
  s.summary          = 'A short description of library.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'YOUR_POD_REPO_MAIN_PAGE_URL'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'YOUR_NAME' => 'YOUR_EMAIL' }
  s.source           = { :git => 'YOUR_POD_REPO_URL', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'library/Classes/**/*'

  # s.resource_bundles = {
  #   'library' => ['library/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
```

If your pod library, depends of another pod library, you can add it on `.podspec` file as dependency like this:

```ruby
s.dependency 'EXTERNAL_POD_LIBRARY'
```

To finish to set up the `.podpsec` add the swift version:

```ruby
s.swift_version = 'SWIFT_VERSION'
```

## Podspec Example

Here is an example of how does the `.podspec` file must be look like:

```ruby
#
# Be sure to run `pod lib lint library.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'library'
  s.version          = '0.1.0'
  s.summary          = 'This is my fist pod library.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: This is a long description.
                       DESC

  s.homepage         = 'https://bitbucket.org/edddea/library'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Edgar Froylan Rodriguez Mondragon' => 'edddea@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/edddea/library.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'library/Classes/**/*'
  s.swift_version = '4.1'

  # s.resource_bundles = {
  #   'library' => ['library/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Alamofire'

end
```

## Validate library

The last step is validate library, for start coding and version the pod library. For validate library type the next line:

```ruby
$ pod lib lint
```

If everything goes ok, a confirmation message will be displayed:

```ruby
-> library (0.1.0)

library passed validation.
```

## Code your library

Now you can start coding your library, a `Example` folder has been created, and a `.workspace` file into it, open it and you will find a `ReplaceMe.swift` under the `Development Pods/library` route.

![ReplaceMe Example](https://s3.amazonaws.com/shareproximateapps/doc/1.png)

At this point you can add all your code into your `Development Pods/library` group folder, instead of `ReplaceMe.swift`. You can remove the `ReplaceMe.swift` file it is only a reference for your library code path. When you start to adding classes to your current `development pod`, you need to create them into `library/Classes` path, for store the pod source code. Select your `library` module target and create it.

![AddingClasses Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/4.png)

## Test your Pod

For test your current working pod, during your development, you can adding classes, write code, then you can test your code.

A test project had created for test your pod code, but you need to install your pod previous to use it on the test project, you need to execute the next command into `Example` folder:

```Ruby
$ pod install
```

Do not forget to make your pod swift classes `public` for the test project access to it, every changes in your pod code must need a `pod install` again for refresh the test project.

In the XCode project workspace you can find a `Tests`  group folder,

![Tests Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/3.png)

And a `Example for library` group folder where you can import your library and access to the pod code for test it, in the `Example for library` case you can test your pod code into a view controller if you need to test directly in the UI.

![ExampleForLibrary Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/2.png)

## Tag your pod library version

Once you finish to code the current version, you need to tag your code for version control, first assign a tag according to your current `.podspec` version, in this case we have version `0.1.0` so we will make a tag according to this version:

```ruby
$ git tag 0.1.0
```

Now, you must push your tag version to the pod library repo:

```ruby
$ git push origin 0.1.0
```

This procedure must be done every time you need to upload new version code.

## Push your pod library to your private pods master repo

All of your pod library are ready to be published, but you need to have an available repo to version control of current and
many other libraries you want to associate to your private repo. Once you have the master repo ready, we associate the repo to our library:

```ruby
$ pod repo add GIVE_A_REPO_NAME YOUR_PRIVATE_PODS_GIT_REPO_URL
```

If you forget the private repo name given, you can verify the repos:

```ruby
$ pod repo
```

Then, push your `.podspec` file to the private master repo:

```ruby
$ pod repo push GIVEN_REPO_NAME library.podspec --verbose
```

## Installation

Pod library now is available from your private repo, for install your pod in a proyect you need to add these lines to the proyect `Podfile`:

```ruby
source 'YOUR_PRIVATE_PODS_GIT_REPO_URL' // To have access to your private pods
source 'https://github.com/CocoaPods/Specs.git' // To have access to all public pods
```

Second line will be needed if you have another public pods dependencies in your proyect.

Then you only have to add your pod library normally in the proyect `Podfile`.

```ruby
pod 'library'
```

And a pod installation required:

```Ruby
$ pod install
```

Thats it, you have your own CocoaPod library ready and tested.

## Author

Edgar Rodriguez, erodriguez@proximateapps.com
